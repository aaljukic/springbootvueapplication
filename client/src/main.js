import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import vuetify from "./plugins/vuetify";
import VueAWN from "vue-awesome-notifications"
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';



Vue.use(VueAWN)
require("vue-awesome-notifications/dist/styles/style.css")


const options = {
  confirmButtonColor: '#2ED565',
  cancelButtonColor: '#FF006C',
};

Vue.use(VueSweetalert2, options);


Vue.config.productionTip = false;



new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");

