import Vue from "vue";
import Vuex from "vuex";

import item from "./modules/item";
import receipt from "./modules/receipt";
import user from "./modules/user";
import colors from "./modules/colors"

import * as actions from "./actions";

Vue.use(Vuex);



export default new Vuex.Store({
  actions,
  modules: {
    item,
    receipt,
    user,
    colors
  },
});
