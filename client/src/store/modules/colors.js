function createMainAppPalette(primaryColor, secondaryColor, tertiaryColor) {
    return {
        primaryColor,
        secondaryColor,
        tertiaryColor
    }
}
function createSideAppPalette(
    secondaryButtonColor,
    tertiaryButtonColor,
    mainTextColor,
    sideTextColor,
    notificationsColor,
    firstHelperColor,
    secondHelperColor,
    thirdHelperColor,
    fourthHelperColor
) {
    return {
        secondaryButtonColor,
        tertiaryButtonColor,
        mainTextColor,
        sideTextColor,
        notificationsColor,
        firstHelperColor,
        secondHelperColor,
        thirdHelperColor,
        fourthHelperColor
    }
}

const state = {
    isDarkTheme: true,
    currentPalette: {
        appTheme: {
            primaryColor: '#181F29',
            secondaryColor: '#0E1319',
            tertiaryColor: '#FF006C'
            // tertiaryColor: '#C8243D'
        },
        other: {
            secondaryButtonColor: "#0268F9",
            tertiaryButtonColor: "#74777C",
            mainTextColor: "#fff",
            sideTextColor: "#74777C",
            notificationsColor: "#6672D8",
            firstHelperColor: '#4AA2E4',
            secondHelperColor: '#7061D4',
            thirdHelperColor: '#282E3E',
            fourthHelperColor: '#EB5757'
        }
    },
};

const mutations = {
    'CHANGE_THEME'(state, typeOfTheme) {
        let generatedPalette = {
            appTheme: {},
            other: {}
        }

        switch (typeOfTheme) {
            case 'DARK':
                generatedPalette.appTheme = createMainAppPalette('#181F29', '#0E1319', '#FF006C');
                generatedPalette.other = createSideAppPalette(
                    '#0268F9',
                    '#74777C',
                    '#fff',
                    '#74777C',
                    '#6672D8',
                    '#4AA2E4',
                    '#7061D4',
                    '#282E3E',
                    '#EB5757'
                );
                break;
            case 'LIGHT':
                generatedPalette.appTheme = createMainAppPalette('#FFF', '#F7FAFE', '#0268F9')
                generatedPalette.other = createSideAppPalette(
                    '#FF006C',
                    '#DBDDE0',
                    '#000',
                    '#74777C',
                    '#2ED565',
                    '#FF006C',
                    '#722EBF',
                    '#282E3E',
                    '#EB5757'
                );
                break;
            default:
                break;
        }
        state.currentPalette = generatedPalette;
    },
    'TOGGLE_THEME_STATE'(state) {
        state.isDarkTheme = !state.isDarkTheme
    }

};

const actions = {
    switchTheme({ commit }) {
        commit("TOGGLE_THEME_STATE")

        state.isDarkTheme == true
            ? commit("CHANGE_THEME", "DARK")
            : commit("CHANGE_THEME", "LIGHT");
    },

};

const getters = {
    getPalette: state => {
        return state.currentPalette;
    },
    getCurrentTheme: state => {
        return state.isDarkTheme;
    },

};

export default {
    state,
    mutations,
    actions,
    getters
}