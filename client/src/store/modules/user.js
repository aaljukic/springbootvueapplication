const state = {
    isUserLoggedIn: true,
    playerName: "Adminko Admić",

};

const mutations = {
    'SIGN_OUT_USER'(state) {
        state.isUserLoggedIn = false;
    },
    'LOGIN_USER'(state) {
        state.isUserLoggedIn = true;
    }
};

const actions = {
    signOutUser({ commit }) {
        commit("SIGN_OUT_USER")
    },
    loginUser({ commit }) {
        commit("LOGIN_USER")
    }
};

const getters = {
    getUserState: state => {
        return state.isUserLoggedIn;
    }
};

export default {
    state,
    mutations,
    actions,
    getters
};