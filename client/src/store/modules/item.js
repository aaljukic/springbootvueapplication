import items from "../../data/items";

const state = {
    items: []
};

const mutations = {
    'INIT_ITEMS' (state, items) {
        state.items = items;
    },

};

const actions = {
    initItems: ({commit}) => {
        commit('INIT_ITEMS', items);
    },
    buyItem: ({commit}, order) => {
        commit('BUY_ITEM', order);
    }

};

const getters = {
    items: state => {
        return state.items;
    }
};

export default {
    state,
    mutations,
    actions,
    getters
};