import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../views/Dashboard/Dashboard.vue";
import Login from "../components/layout/AppLogin/Login"

import Tables from "../views/Tables/Tables.vue";
import TablesView from "../views/Tables/TablesView.vue";
import ItemsTable from "../views/Tables/Item/ItemTable.vue";
import ReceiptsTable from "../views/Tables/Receipt/ReceiptTable.vue";

import Forms from "../views/Forms/Forms.vue";
import FormsView from "../views/Forms/FormsView.vue";

import AddNewItemForm from "../views/Forms/Item/ItemForm.vue";
import AddNewReceiptForm from "../views/Forms/Receipt/ReceiptForm.vue";
import AddNewUserForm from "../views/Forms/User/UserForm.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: Dashboard
  },
  {
    path: "/login",
    component: Login
  },
  {
    path: "/table",
    name: 'tables',
    component: Tables,
    children: [
      {
        path: "",
        name: 'allTables',
        component: TablesView
      },
      {
        path: "items",
        name: 'itemTable',
        component: ItemsTable
      },
      {
        path: "receipts",
        name: 'receiptTable',
        component: ReceiptsTable
      }
    ]
  },
  {
    path: "/form",
    component: Forms,
    name: 'forms',
    children: [
      {
        path: "",
        name: 'allForms',
        component: FormsView
      },
      {
        path: "item",
        name: 'itemForm',
        component: AddNewItemForm
      },
      {
        path: "receipt",
        name: 'receiptForm',
        component: AddNewReceiptForm
      },
      {
        path: "user",
        name: 'userForm',
        component: AddNewUserForm
      }
    ]
  }

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
