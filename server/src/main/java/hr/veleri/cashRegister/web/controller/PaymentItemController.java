package hr.veleri.cashRegister.web.controller;

import hr.veleri.cashRegister.domain.service.PaymentItemService;
import hr.veleri.cashRegister.web.request.CreateNewPaymentItemRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/paymentItem")
public class PaymentItemController {

    @Autowired
    PaymentItemService paymentItemService;

    @GetMapping("/all")
    GeneralResponse getAll() {
        return paymentItemService.getAllPaymentItems();
    }

    @GetMapping("/{id}")
    GeneralResponse getOne(@PathVariable Long id) {
        return paymentItemService.getOnePaymentItem(id);
    }

    @PostMapping("/new")
    GeneralResponse createNew(@RequestBody CreateNewPaymentItemRequest clientRequest) {
        return paymentItemService.createNewPaymentItem(clientRequest);
    }
}
