package hr.veleri.cashRegister.web.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

//@Data
//@NoArgsConstructor
//@RequiredArgsConstructor
public class GeneralResponse<T> {
//    @NonNull
    private T result;

//    @NonNull
    private String message;

//    @NonNull
    private Boolean success;

    protected GeneralResponse() {
    }

    public GeneralResponse(T result, String message, Boolean success) {
        this.result = result;
        this.message = message;
        this.success = success;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
