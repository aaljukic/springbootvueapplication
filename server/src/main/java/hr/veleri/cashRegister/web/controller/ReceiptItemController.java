package hr.veleri.cashRegister.web.controller;

import hr.veleri.cashRegister.domain.service.ReceiptItemService;
import hr.veleri.cashRegister.web.request.CreateNewReceiptItemRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/receiptItem")
public class ReceiptItemController {

    @Autowired
    ReceiptItemService receiptItemService;

    @GetMapping("/all")
    GeneralResponse getAll() {
        return receiptItemService.getAllReceiptItems();
    }

    @GetMapping("/{id}")
    GeneralResponse getOne(@PathVariable Long id) {
        return receiptItemService.getOneReceiptItem(id);
    }

    @PostMapping("/new")
    GeneralResponse createNew(@RequestBody CreateNewReceiptItemRequest clientRequest) {
        return receiptItemService.newReceiptItem(clientRequest);
    }
}
