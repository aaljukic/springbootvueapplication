package hr.veleri.cashRegister.web.controller;

import hr.veleri.cashRegister.domain.service.EmployeeService;
import hr.veleri.cashRegister.web.request.AddNewEmployeeRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/all")
    GeneralResponse getAll() {
        return employeeService.getAllEmployees();
    }

    @GetMapping("/{id}")
    GeneralResponse getOne(@PathVariable Long id) {
        return employeeService.getOneEmployee(id);
    }

    @GetMapping("/number")
    GeneralResponse getNumber() {
        return employeeService.getNumberOfEmployees();
    }

    @PostMapping("/new")
    GeneralResponse createNew(@RequestBody AddNewEmployeeRequest clientRequest) {
        return employeeService.createNewEmployee(clientRequest);
    }
}
