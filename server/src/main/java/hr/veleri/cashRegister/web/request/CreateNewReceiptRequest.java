package hr.veleri.cashRegister.web.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class CreateNewReceiptRequest {

    @NonNull
    private Long employeeId;
}
