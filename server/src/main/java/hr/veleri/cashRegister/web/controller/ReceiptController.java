package hr.veleri.cashRegister.web.controller;

import hr.veleri.cashRegister.domain.service.ReceiptService;
import hr.veleri.cashRegister.web.request.CreateNewReceiptRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/receipt")
public class ReceiptController {

    @Autowired
    ReceiptService receiptService;

    @GetMapping("/all")
    GeneralResponse getAll() {
        return receiptService.getAllReceipts();
    }

    @GetMapping("/{id}")
    GeneralResponse getOne(@PathVariable Long id) {
        return receiptService.getOneReceipt(id);
    }

    @GetMapping("/number")
    GeneralResponse getNumber() {
        return receiptService.getNumberOfReceipts();
    }
}
