package hr.veleri.cashRegister.web.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class AddNewItemRequest {

    @NonNull
    private String code;

    @NonNull
    private String shortName;

    @NonNull
    private String fullName;

    @NonNull
    private String unitOfMeasure;

    @NonNull
    private Double unitPrice;

    @NonNull
    private Double discount;

    @NonNull
    private Double tax;

    @NonNull
    private Integer stock;

    @NonNull
    private String photo;
}
