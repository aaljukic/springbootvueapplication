package hr.veleri.cashRegister.web.request;

import hr.veleri.cashRegister.domain.service.data.request.EmployeeDtoRequest;
import hr.veleri.cashRegister.domain.service.data.request.ItemDtoRequest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class CreateNewReceiptItemRequest {

    @NonNull
    private List<ItemDtoRequest> items;

    @NonNull
    private EmployeeDtoRequest userDetails;

    @NonNull
    private String typeOfPayment;

}
