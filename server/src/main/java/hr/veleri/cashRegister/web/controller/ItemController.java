package hr.veleri.cashRegister.web.controller;

import hr.veleri.cashRegister.domain.service.ItemService;
import hr.veleri.cashRegister.web.request.AddNewItemRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    ItemService itemService;

    @GetMapping("/all")
    GeneralResponse getAll() {
        return itemService.getAllItems();
    }

    @GetMapping("/{id}")
    GeneralResponse getOne(@PathVariable Long id) {
        return itemService.getOneItem(id);
    }

    @PostMapping("/new")
    GeneralResponse createNew(@RequestBody AddNewItemRequest clientRequest) {
        return itemService.createNewItem(clientRequest);
    }

    @GetMapping("/number")
    GeneralResponse getNumber() {
        return itemService.getNumberOfItems();
    }
}
