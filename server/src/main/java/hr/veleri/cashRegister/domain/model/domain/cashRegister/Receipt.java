package hr.veleri.cashRegister.domain.model.domain.cashRegister;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long receiptId;

    @NonNull
    private Long employeeId;

    @NonNull
    private java.util.Date dateOfSignature;

    @NonNull
    private java.util.Date dateOfFiskalizacija;

    @NonNull
    private String zki;

    @NonNull
    private String jir;

    @NonNull
    private Boolean cancellation;

    @NonNull
    private String cancellationReason;

    @NonNull
    private java.util.Date dateCreated;

    @NonNull
    private java.util.Date dateModified;

    @NonNull
    private Boolean active;

    @NonNull
    private String receiptNumber;
}
