package hr.veleri.cashRegister.domain.service.data.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class EmployeeDtoRequest {

    @NonNull
    private Long id;
}
