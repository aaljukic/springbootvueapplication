package hr.veleri.cashRegister.domain.service;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Employee;
import hr.veleri.cashRegister.domain.model.domain.cashRegister.Item;
import hr.veleri.cashRegister.domain.model.domain.cashRegister.Receipt;
import hr.veleri.cashRegister.domain.model.domain.cashRegister.ReceiptItem;
import hr.veleri.cashRegister.domain.repository.cashRegister.EmployeeRepository;
import hr.veleri.cashRegister.domain.repository.cashRegister.ItemRepository;
import hr.veleri.cashRegister.domain.repository.cashRegister.ReceiptItemRepository;
import hr.veleri.cashRegister.domain.repository.cashRegister.ReceiptRepository;
import hr.veleri.cashRegister.domain.service.data.response.EmployeeDtoResponse;
import hr.veleri.cashRegister.domain.service.data.response.ItemDtoResponse;
import hr.veleri.cashRegister.domain.service.data.response.ReceiptDtoResponse;
import hr.veleri.cashRegister.domain.service.data.response.ReceiptItemDtoResponse;
import hr.veleri.cashRegister.web.request.CreateNewReceiptItemRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReceiptItemService {

    @Autowired
    ReceiptItemRepository receiptItemRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    ReceiptService receiptService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ItemService itemService;

    @Autowired
    ItemRepository itemRepository;

    /**
     * Controller methods
     * */
    public GeneralResponse getAllReceiptItems() {
        List<ReceiptItem> allReceiptItems = receiptItemRepository.findAll();
        return checkListIfEmptyWithResponse(allReceiptItems, "All Receipt Items Returned", "No Receipt Items Found" );
    }
    public GeneralResponse<Optional<ReceiptItem>> getOneReceiptItem(Long id) {
        Optional<ReceiptItem> receiptItem = receiptItemRepository.findById(id);
        if (!receiptItem.isPresent()) {
            return new GeneralResponse<>(null, "No Receipt Item Found", false);
        } else {
            return new GeneralResponse<>(receiptItem, "Found Receipt Item", true);
        }
    }

    /**
     * Creating Receipt, ReceiptItems & Creating Response Receipt Dto for client - output invoice
     * */
    public GeneralResponse newReceiptItem(CreateNewReceiptItemRequest newReceiptItemRequest) {
        if (!existenceOfId(newReceiptItemRequest.getUserDetails().getId())) {
            Receipt receipt = newReceipt(newReceiptItemRequest);
            ReceiptDtoResponse receiptDtoResponse = createResponseInvoice(receipt);
            return new GeneralResponse(receiptDtoResponse, "Consumer bought!", true);
        } else {
            Receipt receipt = newReceipt(newReceiptItemRequest);
            ReceiptDtoResponse receiptDtoResponse =  createResponseInvoice(receipt);
            return new GeneralResponse(receiptDtoResponse, "Receipt!", true);
        }
    }

    /**
     * Main methods - Receipt & Invoice
     * */
    private Receipt newReceipt(CreateNewReceiptItemRequest newReceiptItemRequest) {

        Receipt receipt = receiptService.createNewReceipt(newReceiptItemRequest.getUserDetails().getId());
        ArrayList<Long> itemIds = itemService.grabAllItemIds(newReceiptItemRequest.getItems());
        ArrayList<Double> itemQuantities = itemService.grabAllItemQuantities(newReceiptItemRequest.getItems());

        int quantityCounter = 0;
        int ordinalNumberCounter = 1;
        for (Long itemId: itemIds
        ) {
            Optional<Item> item = itemRepository.findById(itemId);
            receiptItemRepository.save(
                    new ReceiptItem(
                            receipt.getReceiptId(),
                            itemId,
                            ordinalNumberCounter,
                            itemQuantities.get(quantityCounter),
                            item.get().getDiscount(),
                            item.get().getUnitPrice(),
                            item.get().getTax(),
                            (itemQuantities.get(quantityCounter) * item.get().getUnitPrice()) * ((100 - item.get().getDiscount()) / 100),
                            new Date(System.currentTimeMillis()),
                            new Date(System.currentTimeMillis()),
                            true
                    )
            );
            quantityCounter++;
            ordinalNumberCounter++;
        }
        return receipt;
    }
    private ReceiptDtoResponse createResponseInvoice(Receipt receipt) {
        EmployeeDtoResponse employee = createEmployeeDtoResponse(receipt.getEmployeeId());
        List<ReceiptItemDtoResponse> receiptItemDtoResponseList = createReceiptItemDtoResponse(receipt.getReceiptId());

        ReceiptDtoResponse receiptDtoResponse = new ReceiptDtoResponse(
                receipt.getReceiptId(),
                employee,
                receipt.getZki(),
                receipt.getJir(),
                receipt.getReceiptNumber(),
                receiptItemDtoResponseList
        );
        return receiptDtoResponse;
    }

    /**
     *  Dto helpers
     * */
    private EmployeeDtoResponse createEmployeeDtoResponse(Long employeeId) {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        String employeeFullName = employee.get().getFullName();
        return new EmployeeDtoResponse(employeeId,employeeFullName);
    }
    private List<ReceiptItemDtoResponse> createReceiptItemDtoResponse(Long receiptId) {
        List<ReceiptItem> allReceiptItems = receiptItemRepository.findByReceiptId(receiptId);
        List<ReceiptItemDtoResponse> receiptItemDtoResponseList = new ArrayList<>();

        for (ReceiptItem receiptItem: allReceiptItems
        ) {
            Optional<Item> item = itemRepository.findById(receiptItem.getItemId());
            ItemDtoResponse itemDtoResponse = new ItemDtoResponse(
                    item.get().getItemId(),
                    item.get().getShortName(),
                    item.get().getFullName(),
                    item.get().getUnitOfMeasure(),
                    item.get().getUnitPrice(),
                    item.get().getPhoto()
            );

            ReceiptItemDtoResponse receiptItemDtoResponse = new ReceiptItemDtoResponse(
                    receiptItem.getReceiptItemId(),
                    receiptId,
                    itemDtoResponse,
                    receiptItem.getQuantity(),
                    receiptItem.getTax(),
                    receiptItem.getAmount()
            );

            receiptItemDtoResponseList.add(receiptItemDtoResponse);
        }
        return receiptItemDtoResponseList;
    }

    /**
     * Validators
     * */
    private boolean isCustomer(CreateNewReceiptItemRequest newReceiptItemRequest) {
        return newReceiptItemRequest.getUserDetails().getId() != null;
    }
    private boolean existenceOfId(Long id) {
        return id != null;
    }
    private GeneralResponse checkListIfEmptyWithResponse(List<ReceiptItem> allReceiptItems, String successMessage, String failureMessage) {
        if (allReceiptItems.isEmpty()) {
            return new GeneralResponse<>(null, failureMessage, false);
        } else {
            return new GeneralResponse<>(allReceiptItems, successMessage, true);
        }
    }
}
