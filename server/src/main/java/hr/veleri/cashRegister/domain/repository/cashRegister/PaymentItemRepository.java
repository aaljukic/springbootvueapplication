package hr.veleri.cashRegister.domain.repository.cashRegister;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.PaymentItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PaymentItemRepository extends JpaRepository<PaymentItem, Long> {
}
