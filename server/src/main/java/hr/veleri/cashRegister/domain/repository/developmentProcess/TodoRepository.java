package hr.veleri.cashRegister.domain.repository.developmentProcess;

import hr.veleri.cashRegister.domain.model.domain.developmentProcess.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
