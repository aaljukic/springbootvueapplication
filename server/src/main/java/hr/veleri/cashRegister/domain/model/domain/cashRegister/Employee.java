package hr.veleri.cashRegister.domain.model.domain.cashRegister;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;

    @NonNull
    private String username;

    @NonNull
    private String email;

    @NonNull
    private String password;

    @NonNull
    private String fullName;

    @NonNull
    private java.util.Date dateCreated;

    @NonNull
    private java.util.Date dateModified;

    @NonNull
    private Boolean active;

    @NonNull
    private Long roleId;


}
