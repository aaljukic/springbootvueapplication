package hr.veleri.cashRegister.domain.service.data.response;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Receipt;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ReceiptItemDtoResponse {

    @NonNull
    private Long id;

    @NonNull
    private Long receiptId;

    @NonNull
    private ItemDtoResponse item;

    @NonNull
    private Double quantity;

    @NonNull
    private Double tax;

    @NonNull
    private Double amount;
}
