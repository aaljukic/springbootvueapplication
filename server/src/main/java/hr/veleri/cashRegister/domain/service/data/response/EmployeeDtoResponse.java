package hr.veleri.cashRegister.domain.service.data.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class EmployeeDtoResponse {

    @NonNull
    private Long id;

    @NonNull
    private String fullName;

}
