package hr.veleri.cashRegister.domain.service.data.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ReceiptDtoResponse {

    @NonNull
    private Long id;

    @NonNull
    private EmployeeDtoResponse employee;

    @NonNull
    private String zki;

    @NonNull
    private String jir;

    @NonNull
    private String receiptNumber;

    @NonNull
    private List<ReceiptItemDtoResponse> receiptItemDtoResponseList;
}
