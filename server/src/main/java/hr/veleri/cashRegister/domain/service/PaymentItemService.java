package hr.veleri.cashRegister.domain.service;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.PaymentItem;
import hr.veleri.cashRegister.domain.repository.cashRegister.PaymentItemRepository;
import hr.veleri.cashRegister.web.request.CreateNewPaymentItemRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentItemService {

    @Autowired
    PaymentItemRepository paymentItemRepository;

    public GeneralResponse<List<PaymentItem>> getAllPaymentItems() {
        List<PaymentItem> allPaymentItems = paymentItemRepository.findAll();
        if (allPaymentItems.isEmpty()) {
            return new GeneralResponse<>(null, "No Payment Items Found", false);
        } else {
            return new GeneralResponse<>(allPaymentItems, "All Payment Items Returned", true);
        }
    }

    public GeneralResponse<Optional<PaymentItem>> getOnePaymentItem(Long id) {
        Optional<PaymentItem> paymentItem = paymentItemRepository.findById(id);
        if (!paymentItem.isPresent()) {
            return new GeneralResponse<>(null, "No Payment Item Found", false);
        } else {
            return new GeneralResponse<>(paymentItem, "Found Payment Item", true);
        }
    }

    public GeneralResponse<PaymentItem> createNewPaymentItem(CreateNewPaymentItemRequest newPaymentItemRequest) {
        return null;
    }

}
