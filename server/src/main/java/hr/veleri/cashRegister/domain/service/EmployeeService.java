package hr.veleri.cashRegister.domain.service;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Employee;
import hr.veleri.cashRegister.domain.repository.cashRegister.EmployeeRepository;
import hr.veleri.cashRegister.web.request.AddNewEmployeeRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public GeneralResponse<List<Employee>> getAllEmployees() {
        List<Employee> allEmployees = employeeRepository.findAll();
        if (allEmployees.isEmpty()) {
            return new GeneralResponse<>(null, "No Employees Found", false);
        } else {
            return new GeneralResponse<>(allEmployees, "All Employees Returned", true);
        }
    }

    public GeneralResponse<Optional<Employee>> getOneEmployee(Long id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        if (!employee.isPresent()) {
            return new GeneralResponse<>(null, "No Employee Found", false);
        } else {
            return new GeneralResponse<>(employee, "Found employee", true);
        }
    }

    public GeneralResponse<Employee> createNewEmployee(AddNewEmployeeRequest newEmployeeRequest) {
        Employee employee = employeeRepository.findByUsername(newEmployeeRequest.getUsername());
        if (doesExist(employee)) {
            return new GeneralResponse<>(null, "Employee with that username already exist", false);
        } else {
            Employee createdNewEmployee = employeeRepository.save(
                    new Employee(
                            newEmployeeRequest.getUsername(),
                            newEmployeeRequest.getEmail(),
                            newEmployeeRequest.getPassword(),
                            newEmployeeRequest.getFullName(),
                            new Date(System.currentTimeMillis()),
                            new Date(System.currentTimeMillis()),
                            true,
                            newEmployeeRequest.getRoleId()

                    )
            );
            return new GeneralResponse<>(createdNewEmployee, "New Employee Created", true);
        }
    }

    private <T> boolean doesExist(T isNull) {
        return isNull != null;
    }

    public GeneralResponse getNumberOfEmployees() {
        return new GeneralResponse<>(employeeRepository.findAll().size(), "Ok", true);
    }
}
