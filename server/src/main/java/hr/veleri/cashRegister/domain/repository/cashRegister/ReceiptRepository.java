package hr.veleri.cashRegister.domain.repository.cashRegister;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface ReceiptRepository extends JpaRepository<Receipt, Long> {
    Optional<Receipt> findByReceiptNumber(String receiptNumber);

    Receipt findTopByOrderByReceiptIdDesc();
}
