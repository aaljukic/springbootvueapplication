package hr.veleri.cashRegister.domain.service;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Item;
import hr.veleri.cashRegister.domain.repository.cashRegister.ItemRepository;
import hr.veleri.cashRegister.domain.service.data.request.ItemDtoRequest;
import hr.veleri.cashRegister.web.request.AddNewItemRequest;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepository;

    public GeneralResponse<List<Item>> getAllItems() {
        List<Item> allItems = itemRepository.findAll();
        if (allItems.isEmpty()) {
            return new GeneralResponse<>(null, "No Items Found", false);
        } else {
            return new GeneralResponse<>(allItems, "All Items Returned", true);
        }
    }

    public GeneralResponse getNumberOfItems() {
        return new GeneralResponse<>(itemRepository.findAll().size(), "Ok", true);
    }

    public GeneralResponse<Optional<Item>> getOneItem(Long id) {
        Optional<Item> item = itemRepository.findById(id);
        if (!item.isPresent()) {
            return new GeneralResponse<>(null, "No Item Found", false);
        } else {
            return new GeneralResponse<>(item, "Found item", true);
        }
    }

    public GeneralResponse<Item> createNewItem(AddNewItemRequest newItemRequest) {
        return null;
    }

    public ArrayList<Long> grabAllItemIds(List<ItemDtoRequest> items) {
        ArrayList<Long> allItemIds = new ArrayList<>();
        for (ItemDtoRequest item:
                items) {
            allItemIds.add(item.getId());
        }
        return allItemIds;
    }

    public ArrayList<Double> grabAllItemQuantities(List<ItemDtoRequest> items) {
        ArrayList<Double> allItemQuantities = new ArrayList<>();
        for (ItemDtoRequest item:
                items) {
            allItemQuantities.add(item.getQuantity());
        }
        return allItemQuantities;
    }

}
