package hr.veleri.cashRegister.domain.model.domain.cashRegister;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long paymentItemId;

    @NonNull
    private Long receiptId;

    @NonNull
    private Integer ordinalNumber;

    @NonNull
    private String typeOfPayment;

    @NonNull
    private java.util.Date dateCreated;

    @NonNull
    private java.util.Date dateModified;

    @NonNull
    private Boolean active;
}
