package hr.veleri.cashRegister.domain.model.domain.cashRegister;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long itemId;

    @NonNull
    private String code;

    @NonNull
    private String shortName;

    @NonNull
    private String fullName;

    @NonNull
    private String unitOfMeasure;

    @NonNull
    private Double unitPrice;

    @NonNull
    private Double discount;

    @NonNull
    private Double tax;

    @NonNull
    private Integer stock;

    @NonNull
    private String photo;

    @NonNull
    private java.util.Date dateCreated;

    @NonNull
    private java.util.Date dateModified;

    @NonNull
    private Boolean active;
}
