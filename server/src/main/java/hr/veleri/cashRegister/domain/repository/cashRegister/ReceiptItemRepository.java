package hr.veleri.cashRegister.domain.repository.cashRegister;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.ReceiptItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ReceiptItemRepository extends JpaRepository<ReceiptItem, Long> {
    List<ReceiptItem> findByReceiptId(Long receiptId);
}
