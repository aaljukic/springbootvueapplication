package hr.veleri.cashRegister.domain.repository.cashRegister;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Employee;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByUsername(@NonNull String username);
}
