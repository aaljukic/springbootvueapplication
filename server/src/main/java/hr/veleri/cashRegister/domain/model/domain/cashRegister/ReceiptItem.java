package hr.veleri.cashRegister.domain.model.domain.cashRegister;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ReceiptItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long receiptItemId;

    @NonNull
    private Long receiptId;

    @NonNull
    private Long itemId;

    @NonNull
    private Integer ordinalNumber;

    @NonNull
    private Double quantity;

    @NonNull
    private Double discount;

    @NonNull
    private Double unitPrice;

    @NonNull
    private Double tax;

    @NonNull
    private Double amount;

    @NonNull
    private java.util.Date dateCreated;

    @NonNull
    private java.util.Date dateModified;

    @NonNull
    private Boolean active;
}
