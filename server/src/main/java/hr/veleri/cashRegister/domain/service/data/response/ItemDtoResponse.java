package hr.veleri.cashRegister.domain.service.data.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ItemDtoResponse {

    @NonNull
    private Long id;

    @NonNull
    private String shortName;

    @NonNull
    private String fullName;

    @NonNull
    private String unitOfMeasure;

    @NonNull
    private Double unitPrice;

    @NonNull
    private String photo;
}
