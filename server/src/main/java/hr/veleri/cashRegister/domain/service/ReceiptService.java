package hr.veleri.cashRegister.domain.service;

import hr.veleri.cashRegister.domain.model.domain.cashRegister.Employee;
import hr.veleri.cashRegister.domain.model.domain.cashRegister.Receipt;
import hr.veleri.cashRegister.domain.repository.cashRegister.EmployeeRepository;
import hr.veleri.cashRegister.domain.repository.cashRegister.ReceiptRepository;
import hr.veleri.cashRegister.web.response.GeneralResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class ReceiptService {

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    public GeneralResponse<List<Receipt>> getAllReceipts() {
        List<Receipt> allReceipts = receiptRepository.findAll();
        if (allReceipts.isEmpty()) {
            return new GeneralResponse<>(null, "No Receipts Found", false);
        } else {
            return new GeneralResponse<>(allReceipts, "All Receipts Returned", true);
        }
    }


    public GeneralResponse getNumberOfReceipts() {
        return new GeneralResponse<>(receiptRepository.findAll().size(), "Ok", true);
    }

    public GeneralResponse<Optional<Receipt>> getOneReceipt(Long id) {
        Optional<Receipt> receipt = receiptRepository.findById(id);
        if (!receipt.isPresent()) {
            return new GeneralResponse<>(null, "No Receipt Found", false);
        } else {
            return new GeneralResponse<>(receipt, "Found Receipt", true);
        }
    }

    public Optional<Receipt> getOneReceipt(String receiptNumber) {
        return receiptRepository.findByReceiptNumber(receiptNumber);
    }

    private int counter = 0;
    Receipt createNewReceipt(Long employeeId) {
        Receipt newReceipt = new Receipt();
        newReceipt.setEmployeeId(employeeId);
        newReceipt.setDateOfSignature(new Date(System.currentTimeMillis()));
        newReceipt.setDateOfFiskalizacija(new Date(System.currentTimeMillis()));
        newReceipt.setZki(generateRandomString());
        newReceipt.setJir(generateRandomString());
        newReceipt.setCancellation(false);
        newReceipt.setCancellationReason("");
        newReceipt.setDateCreated(new Date(System.currentTimeMillis()));
        newReceipt.setDateModified(new Date(System.currentTimeMillis()));
        newReceipt.setActive(false);

        // broj novog racuna
        Date newReceiptReceiptNumber = newReceipt.getDateOfFiskalizacija();
        String dateOfNewReceiptNumber = new SimpleDateFormat("yyyy/MMdd").format(newReceiptReceiptNumber);

        // prethodni racun
        Receipt lastReceipt = receiptRepository.findTopByOrderByReceiptIdDesc();
        // Split
        String lastReceiptReceiptNumber = lastReceipt.getReceiptNumber();
        String[] parts = lastReceiptReceiptNumber.split("-");
        // Prvi dio samo za datum
        String lastReceiptDate = parts[0];
        // Drugi dio za counter
        String lastReceiptCounter = parts[1];

        int counterFromLastReceipt = Integer.parseInt(lastReceiptCounter);

        if (lastReceiptDate.equals(dateOfNewReceiptNumber)) {

            // ovdje provjeravamo da li drugi dio je dosao do nekog broja
            if (counterFromLastReceipt > 0) {
                counter = counterFromLastReceipt + 1;

                String stringCounter = String.format("%05d", counter);

                newReceipt.setReceiptNumber(dateOfNewReceiptNumber + "-" + stringCounter);

                return receiptRepository.save(newReceipt);

            } else {
                counter++;

                String stringCounter = String.format("%05d", counter);

                newReceipt.setReceiptNumber(dateOfNewReceiptNumber + "-" + stringCounter);

                return receiptRepository.save(newReceipt);
            }

        } else {
            counter = 0;

            String stringCounter = String.format("%05d", counter);

            newReceipt.setReceiptNumber(dateOfNewReceiptNumber + "-" + stringCounter);

            return receiptRepository.save(newReceipt);
        }
    }

    private String generateRandomString() {
        int leftLimit = 65;
        int rightLimit = 122;
        int length = 18;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }
}
